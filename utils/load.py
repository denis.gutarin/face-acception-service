with open("vectors.tsv") as file:
    lines = file.readlines()

import sqlalchemy as db

session = db.create_engine("postgres://postgres:postgres@localhost:5432/vision")

lines = lines[1:]
for x in lines:
    fields = x.split()
    fields[1] = bytes.fromhex(fields[1].replace("E'\\\\x", "").rstrip("'"))
    exec = session.execute("insert into vectors(image,	status,	coords,	cluster) values (%s, %s, %s, %s)",
                           (fields[1], 0, fields[3], fields[4]))
