import datetime
import io
from flask import request, render_template, send_file
from webapp import db
from webapp import app
from webapp.models import Vector


@app.route("/photo/<string:cluster>", methods=["GET"])
def photo(cluster):
    photo = (db.session.query(Vector.image)
             .filter(Vector.cluster == cluster)
             .first())
    if photo:
        file = io.BytesIO(photo[0])
        return send_file(file, mimetype="image/jpeg")
    else:
        return None


@app.route("/", methods=["GET"])
def index():
    counter = db.func.count(db.func.distinct(Vector.id))
    vectors = (db.session.query(Vector.cluster, counter)
               .filter(Vector.status == 0)
               .group_by(Vector.cluster)
               .order_by(db.desc(counter))
               .all())

    all = sum(x[1] for x in vectors)

    return render_template("index.html", variants=vectors, all=all)


def update_vectors_status(cluster, status):
    values = {Vector.status: status, Vector.timestamp: db.func.now()}
    result = (db.session.query(Vector)
              .filter(Vector.status == 0)
              .filter(Vector.cluster == cluster)
              .update(values, synchronize_session=False))
    db.session.commit()


def update_another_vectors_status(cluster, status):
    values = {Vector.status: -1, Vector.timestamp: db.func.now()}
    limit_timestamp = datetime.datetime.utcnow() - datetime.timedelta(minutes=1)
    result = (db.session.query(Vector)
              .filter(Vector.timestamp < limit_timestamp)
              .filter(Vector.status == 0)
              .filter(Vector.custer != cluster)
              .delete())
    db.session.commit()
    result = (db.session.query(Vector)
              .filter(Vector.status == 0)
              .filter(Vector.custer != cluster)
              .update(values, synchronize_session=False))
    db.session.commit()


@app.route("/right/<string:cluster>", methods=["GET"])
def right_person(cluster):
    update_vectors_status(cluster, 1)
    update_another_vectors_status(cluster, -1)
    return index()
